<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{YPp]3V6%&WA4^xv_f;4w{+Q<,0q$x&?:w_5h5*_WGB<JL+7oFD3K_X7yRRi-+>~');
define('SECURE_AUTH_KEY',  ')xs;pLc!m*=e=b+P+x^w!:$X-Lj$epkD?/zZ/zQ:N0`3UNv~<)R! w`pfS+CW_p@');
define('LOGGED_IN_KEY',    'Swq|<I<6Udx6Ph|F3/ qP7+fJsPJU%H *S|kd|^(#9KFfD9=PNx0vY1PlYt}y<sd');
define('NONCE_KEY',        '2~+!PZcmOGU.|?W$c8<{>REki$+t|xO%(v_JLsfK7;C{6KhX,.qi08)Y%bv1g1-T');
define('AUTH_SALT',        'R;Eo<hl(=_kFw(k+fd*PW(ZdgK,lc (O Gu{@+qkk/l<4Q60qHA@{#f9eMz27_hW');
define('SECURE_AUTH_SALT', '(Iw]A)zhnxe(hX7_@^{^ms60p@iYlC)]j&|<Y#iY|qYn5u~+8Q2 O`J(X4QEeDG|');
define('LOGGED_IN_SALT',   'h32MjSaw60q <!]K5DYLH1# :[-Vt&Ty4=;PcijFz/tE<SRwZ4r^o/+nJ7a7iqb/');
define('NONCE_SALT',       '-]0T |#Z|xYIj;M _Bf?OMaa+!s?u(FJ`4;|MCG-(V(Ei`/jg4irydalvNTkzo9u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


