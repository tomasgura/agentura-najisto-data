=== Smartsupp Live Chat ===
Contributors: tomwaw
Donate link: 
Tags: livechat, live chat, smartsupp, woocommerce
Requires at least: 3.6.0
Tested up to: 3.9.2
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds Smartsupp Live Chat code to your Wordpress.

== Description ==

Česky popis tohoto plugin a návod najdete [zde](http://www.smartsupp.com/cs/help/wordpress).

This plugin adds Smartsupp Live Chat code to your Wordpress.

Features:

* adds Smartsupp Live Chat code to your website
* adds ability to send variables to your Smartsupp dashboard so you can see informations about your customer while communicating with them

Supported variables:

* Wordpress: display name, username, role, e-mail
* Woocommerce: spendings, amount of orders, billing location


== Installation ==

= Installation via Wordpress dashboard = 

1. In your Admin, go to menu Plugins > Add
2. Search for *smartsupp live chat*
3. Click to install
4. Activate the plugin

= Manual installation = 

1. Download the plugin from Wordpress Plugins repository
2. Unzip `smartsupp.zip` and copy `smartsupp` folder to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

1. Plugin settings

== Changelog ==

= 0.1 =
* Inital version of the Plugin
