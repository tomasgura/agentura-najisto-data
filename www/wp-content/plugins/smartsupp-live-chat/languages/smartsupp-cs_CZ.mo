��          �   %   �      P     Q     b     �     �     �                     )     .     5  #   :     ^  (   ~     �     �     �     �                &     E     K     T     j  �  ~     .  �   C     �     �     �     �     �          $     +     7  )   <  (   f  -   �     �     �     �  !        6     H      \     }     �     �     �                                                       	                                            
                          Billing Location By enabling this option you will be able to see selected variables in your Smartsupp dashboard. More info <a href="#">here</a>. Dashboard Variables E-mail Email Enable chat Enable variables Location Name Orders Role Shows customer's billing locaition. Shows customer's orders amount. Shows how much money customer has spent. Shows user's display name. Shows user's email. Shows user's role. Shows user's username. Smartsupp Chat ID Smartsupp Live Chat Smartsupp Live Chat - Settings Spent Username Woocommerce variables Wordpress variables Project-Id-Version: smartsupp
POT-Creation-Date: 2014-08-27 10:47+0100
PO-Revision-Date: 2014-08-27 15:19+0100
Last-Translator: Tom Wawrosz
Language-Team: 
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: ..
 Fakturační adresa. Povolením této možnosti budete schopni vidět vybrané proměnné přímo v Smartsupp dashboardu. Více informací <a href="#">zde</a>. Dashboard proměnné E-mail Email Povolit chat Povolit proměnné Fakturační adresa Jméno Objednávky Role Zobrazí fakturační adresu zákazníka. Zobrazí počet objednávek zákazníka. Zobrazí obrat zákazníka na Vašém eshopu. Zobrazí uživatelovo jméno. Zobrazí email uživatele. Zobrazí úroveň uživatele. Zobrazí přezdvívku uživatele. Smartsupp Chat ID Smartsupp Live Chat Smartsupp Live Chat - Nastavení Obrat Přihlašovací jméno Woocommerce proměnné Wordpress proměnné 